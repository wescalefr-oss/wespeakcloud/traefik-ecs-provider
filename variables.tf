variable "vpc_id" {
  default = "vpc-0a2ece3be206cf27c"
}

variable "namespace" {
  default = "traefik"
}

variable "subnets" {
  type    = list
  default = ["subnet-00ac380a9c8b00c0e", "subnet-0f9c8928969338075"]
}

variable "access_key" {
  default = "AKIAXXXXXXXXXXXXXXXX"
}

variable "secret_access_key_id" {
  default = "arn:aws:secretsmanager:eu-west-1:1234567890:secret:traefik-secret_access_key_value-esBK5Z"
}